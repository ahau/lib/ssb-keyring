const { keySchemes } = require('private-group-spec')
const { SecretKey } = require('ssb-private-group-keys')

const { toBuffer } = require('../util')

const SELF_KEY = 'own_key'
// leave string like this for v1 ssb-keyring compatability

module.exports = function DMSelf (db) {
  let cache // a single `info`

  return {
    load (cb) {
      readPersisted((err, info) => {
        if (err && err.notFound) create(cb)
        else if (err) cb(err)
        else {
          cache = info
          cb(null)
        }
      })
    },

    set,

    get () {
      return cache
    }
  }

  function create (cb) {
    const key = new SecretKey().toBuffer()
    set({ key }, (err) => {
      if (err) return cb(err)
      cb(null, cache)
    })
  }

  // There can only be one self DM key
  function set (info, cb) {
    try {
      // 32 bytes, where 32 === sodium-universal's crypto_secretbox_KEYBYTES
      info.key = toBuffer(info.key, 32)
    } catch (e) { return cb(e) }

    info = {
      key: info.key,
      scheme: info.scheme || keySchemes.feed_id_self // optional
    }
    cache = info

    db.put(SELF_KEY, info, cb)
  }

  function readPersisted (cb) {
    db.get(SELF_KEY, (err, value) => {
      if (err) return cb(err)
      cb(null, value)
    })
  }
}
