const { keySchemes } = require('private-group-spec')
const lFind = require('lodash.find')

const { toBuffer, isMsg, isSameKey } = require('../util')

// for processing of added group info
function processInfo (groupInfo, addInfo, cb) {
  if (addInfo.key) {
    const newKey = {}
    try {
      // 32 bytes, where 32 === sodium-universal's crypto_secretbox_KEYBYTES
      newKey.key = toBuffer(addInfo.key, 32)
    } catch (e) {
      return cb(e)
    }
    // TODO check scheme is valid in some way?
    newKey.scheme = addInfo.scheme ?? keySchemes.private_group

    if (!groupInfo.readKeys) groupInfo.readKeys = []
    if (!lFind(
      groupInfo.readKeys,
      readKey => isSameKey(readKey.key, newKey.key) && readKey.scheme === newKey.scheme
    )) {
      groupInfo.readKeys.push(newKey)
    }

    // only set the writeKey automatically if there isn't already one
    if (!groupInfo.writeKey) {
      groupInfo.writeKey = newKey
    }

    // calling group.add with a key unexcludes you
    // since we're also using this in tribes1 where we don't rotate keys on exclusion
    delete groupInfo.excluded
  }

  if (addInfo.root) {
    if (!isMsg(addInfo.root)) {
      return cb(
        new Error(`expected info.root to be MsgId, got ${addInfo.root}`)
      )
    }
    groupInfo.root = addInfo.root
  }

  cb()
}

module.exports = {
  processInfo
}
