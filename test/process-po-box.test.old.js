const test = require('tape')
const { promisify: p } = require('util')
const { generate } = require('ssb-keys')

const keyRing = require('../')
const { tmpPath, POBox } = require('./helpers')

test('processAddMember', t => {
  const myKeys = generate() // some ssb-keys

  const tests = [
    async () => {
      const DESCRIPTION = 'processAddMember (brand new poBox)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)
      const { id: poBoxId, key } = POBox()

      keyStore.processPOBox({ poBoxId, poBoxKey: key }, (err, result) => {
        if (err) throw err
        t.true(result, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'processAddMember (exiting poBox)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)
      const { id: poBoxId, key } = POBox()

      keyStore.poBox.register(poBoxId, { key }, (err) => {
        if (err) throw err

        keyStore.processPOBox({ poBoxId, poBoxKey: key }, (err, result) => {
          if (err) throw err
          t.false(result, DESCRIPTION)
          keyStore.close()
        })
      })
    }
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
