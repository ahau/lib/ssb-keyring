const test = require('tape')
const { promisify: p } = require('util')
const { generate } = require('ssb-keys')

const keyRing = require('../')
const { tmpPath, GroupKey, GroupId, FeedId, MsgId } = require('./helpers')

test('processAddMember', t => {
  const myKeys = generate() // some ssb-keys

  const tests = [
    async () => {
      const DESCRIPTION = 'processAddMember (brand new group and members)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)

      const authors = [FeedId()]
      const args = {
        groupId: GroupId(),
        groupKey: GroupKey(),
        root: MsgId(),
        authors
      }

      keyStore.processAddMember(args, (err, result) => {
        if (err) throw err
        t.deepEqual(result, authors, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'processAddMember (adding authors to existing group)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)

      const args = {
        groupId: GroupId(),
        groupKey: GroupKey(),
        root: MsgId(),
        authors: [FeedId()]
      }
      const newAuthor = FeedId()

      keyStore.processAddMember(args, (err) => {
        if (err) throw err
        // simulate duplicate, slightly different group/add-member
        args.authors.push(newAuthor)
        keyStore.processAddMember(args, (err, result) => {
          if (err) throw err
          t.deepEqual(result, [newAuthor], DESCRIPTION)
          keyStore.close()
        })
      })
    },

    async () => {
      const DESCRIPTION = 'processAddMember (adding authors to existing group - no new info)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)

      const args = {
        groupId: GroupId(),
        groupKey: GroupKey(),
        root: MsgId(),
        authors: [FeedId()]
      }

      keyStore.processAddMember(args, (err) => {
        if (err) throw err
        // simulate duplicate, slightly different group/add-member
        args.authors.push(FeedId())
        keyStore.processAddMember(args, (err) => {
          if (err) throw err

          // run the same add again (so no new group/ authors)
          // running 3 processAddMember tests how persistence is behaving (maybe?)
          keyStore.processAddMember(args, (err, result) => {
            if (err) throw err
            t.deepEqual(result, [], DESCRIPTION)
            keyStore.close()
          })
        })
      })
    },

    async () => {
      const DESCRIPTION = 'processAddMember (same groupId with different groupKey Errors)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)

      const args = {
        groupId: GroupId(),
        groupKey: GroupKey(),
        root: MsgId(),
        authors: [FeedId()]
      }

      keyStore.processAddMember(args, (err) => {
        if (err) throw err
        // simulate malicious add
        args.groupKey = GroupKey()
        keyStore.processAddMember(args, (err) => {
          t.match(
            err.message,
            /groupId [^\s]+ already registered with a different groupKey/,
            DESCRIPTION
          )
          keyStore.close()
        })
      })
    },

    async () => {
      const DESCRIPTION = 'processAddMember (same groupId with different root Errors)'

      const keyStore = await p(keyRing)(tmpPath(), myKeys)

      const args = {
        groupId: GroupId(),
        groupKey: GroupKey(),
        root: MsgId(),
        authors: [FeedId()]
      }

      keyStore.processAddMember(args, (err) => {
        if (err) throw err
        // simulate malicious add
        args.root = MsgId()
        keyStore.processAddMember(args, (err) => {
          t.match(
            err.message,
            /groupId [^\s]+ already registered with a different root/,
            DESCRIPTION
          )
          keyStore.close()
        })
      })
    }
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
