/* eslint-disable camelcase */

const test = require('tape')
const BFE = require('ssb-bfe')
const na = require('sodium-universal')
const { promisify: p } = require('util')
const ssbKeys = require('ssb-keys')

const Keyring = require('../')
// specifically 1.1.0, the last one ahau uses before the keyring format migration
const OldKeyring = require('ssb-keyring')
const { tmpPath, POBox } = require('./helpers')
const { keySchemes } = require('private-group-spec')

test('keyring.poBox', async t => {
  const path = tmpPath()
  let keyring = await Keyring(path)

  let DESCRIPTION
  const { id: poBoxId, key } = POBox()

  DESCRIPTION = 'poBoxIdBFE must be 2 + na.crypto_scalarmult_SCALARBYTES long'
  const poBoxIdBFE = BFE.encode(poBoxId)
  t.equals(poBoxIdBFE.length, 2 + na.crypto_scalarmult_SCALARBYTES, DESCRIPTION)

  DESCRIPTION = 'poBox key must be na.crypto_scalarmult_SCALARBYTES long'
  t.equals(key.length, na.crypto_scalarmult_SCALARBYTES, DESCRIPTION)

  /* keys.poBox.add(poBoxId, keyInfo, cb) */
  DESCRIPTION = 'poBox.add, bad poBoxId => error'
  await p(keyring.poBox.add)('junk', { key: POBox() })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected a poBoxId/, DESCRIPTION)) // ✓

  DESCRIPTION = 'poBox.add, bad info.key => error'
  await p(keyring.poBox.add)(poBoxId, { key: 'junk' })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected buffer of length 32/, DESCRIPTION)) // ✓

  // TODO associate with groupId?
  // DESCRIPTION = 'poBox.add, bad info.root => error'
  // await keyring.poBox.add(Group().id, { key: GroupKey(), root: 'dog' })
  //   .then(res => t.fail(DESCRIPTION))
  //   .catch(err => t.match(err && err.message, /expected info.root to be MsgId/, DESCRIPTION)) // ✓

  DESCRIPTION = 'poBox.add, works! => true'
  const info = { key }
  const res = keyring.poBox.add(poBoxId, info, (err) => err && t.fail(err, DESCRIPTION))
  t.equals(res, true, DESCRIPTION) // ✓

  DESCRIPTION = 'poBox.add, same key again => false'
  const res2 = keyring.poBox.add(poBoxId, info, (err) => err && t.fail(err, DESCRIPTION))
  t.equals(res2, false, DESCRIPTION) // ✓

  /* keys.poBox.get(poBoxId) */
  DESCRIPTION = 'poBox.get'
  const expected = info
  t.deepEqual(keyring.poBox.get(poBoxId), expected, DESCRIPTION)
  t.deepEqual(keyring.poBox.get(POBox().id), undefined, DESCRIPTION + ' (unknown poBoxId)')

  DESCRIPTION = '...persists'
  await keyring.close()
  keyring = await Keyring(path)
  t.deepEqual(keyring.poBox.get(poBoxId), expected, DESCRIPTION)

  /* keys.poBox.has(poBoxId) */
  DESCRIPTION = 'poBox.has'
  t.true(keyring.poBox.has(poBoxId), DESCRIPTION)
  t.false(keyring.poBox.has(POBox().id), DESCRIPTION + ' (unknown poBoxId)')

  /* keys.poBox.list() */
  DESCRIPTION = 'poBox.list'
  t.deepEqual(keyring.poBox.list(), [poBoxId], DESCRIPTION)

  keyring.close()
  t.end()
})

test('loading from old pobox db version works', async t => {
  const path = tmpPath()

  const { id: poBoxId, key } = POBox()
  const poboxInfo = {
    key,
    scheme: keySchemes.po_box
  }
  const feedKeys = ssbKeys.generate()

  const oldKeyring = await p(OldKeyring)(path, feedKeys)

  await p(oldKeyring.poBox.register)(poBoxId, { key })

  t.deepEqual(oldKeyring.poBox.get(poBoxId),
    poboxInfo,
    'pobox stored by old keyring version'
  )

  await p(oldKeyring.close)()

  const keyring = await Keyring(path)

  t.deepEqual(keyring.poBox.get(poBoxId),
    poboxInfo,
    'pobox stored with old keyring can be gotten by new version in same format'
  )
})
