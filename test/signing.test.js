/* eslint-disable camelcase */

const test = require('tape')
const ssbKeys = require('ssb-keys')
const { promisify: p } = require('util')

const Keyring = require('../')
const { tmpPath } = require('./helpers')

test('keyring.signing', async t => {
  const path = tmpPath()
  let keyring = await Keyring(path)

  let DESCRIPTION
  const testKeys = ssbKeys.generate()

  /* keys.signing.add(keys, cb) */
  DESCRIPTION = 'signing.add, bad keys.id => error'
  await p(keyring.signing.add)({ junk: true })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /keys.id is not defined/, DESCRIPTION)) // ✓

  DESCRIPTION = 'signing.add, bad keys.curve => error'
  await p(keyring.signing.add)({ ...testKeys, curve: undefined })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /keys.curve is not defined/, DESCRIPTION)) // ✓

  DESCRIPTION = 'signing.add, bad keys.public => error'
  await p(keyring.signing.add)({ ...testKeys, public: undefined })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /keys.public key is not defined/, DESCRIPTION)) // ✓

  DESCRIPTION = 'signing.add, bad keys.private => error'
  await p(keyring.signing.add)({ ...testKeys, private: undefined })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /keys.private key is not defined/, DESCRIPTION)) // ✓

  DESCRIPTION = 'signing.add, works! => true'
  const res = keyring.signing.add(testKeys, (err) => err && t.fail(err, DESCRIPTION))
  t.equals(res, true, DESCRIPTION) // ✓

  DESCRIPTION = 'signing.add, same key again => false'
  const res2 = keyring.signing.add(testKeys, (err) => err && t.fail(err, DESCRIPTION))
  t.equals(res2, false, DESCRIPTION) // ✓

  DESCRIPTION = 'signing.addNamed root => true'
  const res3 = keyring.signing.addNamed('root', testKeys, (err) => err && t.fail(err, DESCRIPTION))
  t.equals(res3, true, DESCRIPTION) // ✓

  /* keys.signing.get(id) */
  DESCRIPTION = 'signing.get'
  t.deepEqual(keyring.signing.get(testKeys.id), testKeys, DESCRIPTION)

  DESCRIPTION = '...persists'
  await keyring.close()
  keyring = await Keyring(path)
  t.deepEqual(keyring.signing.get(testKeys.id), testKeys, DESCRIPTION)

  /* keys.signing.has(id) */
  DESCRIPTION = 'signing.has'
  t.true(keyring.signing.has(testKeys.id), DESCRIPTION)

  keyring.close()
  t.end()
})
