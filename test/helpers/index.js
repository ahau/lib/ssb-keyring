const crypto = require('crypto')
const bfe = require('ssb-bfe')
const { SecretKey, DHKeys } = require('ssb-private-group-keys')

const mockTF = (type, format) => {
  const buf = Buffer.concat([
    bfe.toTF(type, format),
    crypto.randomBytes(32)
  ])
  return bfe.decode(buf)
}

const formats = Object.keys(bfe.bfeNamedTypes.feed.formats)
  .filter(format => format !== 'bamboo') // doesn't have clear URI/sigil support

let i = 0 // used for tmpPath to avoid collisions if tests are very fast

module.exports = {
  FeedId: () => mockTF('feed', 'classic'),
  MsgId: () => mockTF('message', 'classic'),
  GroupId: () => mockTF('message', 'cloaked'),
  GroupURIId: () => mockTF('identity', 'group'),
  GroupKey: () => new SecretKey().toBuffer(),

  Group: () => {
    return {
      id: mockTF('message', 'cloaked'),
      // NOTE techinically the id should be derived from key + root, but meh
      key: new SecretKey().toBuffer(),
      root: mockTF('message', 'classic')
    }
  },

  POBox: () => {
    const poBox = new DHKeys().generate()
    return {
      id: bfe.decode(
        Buffer.concat([
          bfe.toTF('identity', 'po-box'),
          poBox.toBuffer().public
        ])
      ),
      key: poBox.toBuffer().secret
    }
  },
  formats,

  tmpPath: () => `/tmp/ssb-keyring-${Date.now()}-${i++}`
}
